﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMultiValueConverter
{
   public interface IMultiValueConverter
    {
        object Converter(object value, Type targetType, object parameter, string language);
        object ConverterBack(object value, Type targetType, object parameter, string language);
    }
}
